"""
The module containing all the dataclasses.
"""

from dataclasses import dataclass
from typing import Dict, List, Optional


@dataclass(frozen=True, eq=True)
class LatencyStat:
    """The dataclass for latency statistics."""

    rtt_min: Optional[float]
    rtt_max: Optional[float]
    rtt_avg: Optional[float]
    rtt_mdev: Optional[float]

    loss_avg: float  # Percent of unsuccessful communications


@dataclass(frozen=True, eq=True)
class Host:
    """The dataclass used to represent an host."""

    name: str
    address: str


@dataclass(frozen=True, eq=True)
class PingResult:
    """The dataclass used to reprensent the result of ping for a single host."""

    host: Host
    latency_stat: LatencyStat
    protocol: str


@dataclass(frozen=True)
class PingerConfig:
    """The dataclass used to reprensent the parsed config of the pinger."""

    current_host: Host
    group_dict: Dict[Host, str]
    hosts: List[Host]
