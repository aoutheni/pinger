"""
Handling all the logic to send data to carbon.
"""

from typing import List, Tuple
import time
import struct
import pickle
import logging

import asyncio

from pinger.dataclass import PingResult, PingerConfig


def build_data_for_carbon(
    config: PingerConfig,
    ping_results: List[PingResult],
    timestamp: float,
    ip_family: str,
):
    """Transform a list of PingResult to a list of data that can be sent to
    carbon.
    """
    data: List[Tuple[str, Tuple[float, float]]] = []

    for ping_result in ping_results:
        group = config.group_dict[ping_result.host]
        carbon_prefix = (
            f"pinger.{group}.{config.current_host.name}."
            + f"{ping_result.host.name}.{ip_family}.{ping_result.protocol}."
        )

        if (
            ping_result.latency_stat.rtt_min
            and ping_result.latency_stat.rtt_max
            and ping_result.latency_stat.rtt_avg
            and ping_result.latency_stat.rtt_mdev
        ):
            data.append(
                (
                    carbon_prefix + "rtt.min",
                    (timestamp, ping_result.latency_stat.rtt_min),
                )
            )
            data.append(
                (
                    carbon_prefix + "rtt.max",
                    (timestamp, ping_result.latency_stat.rtt_max),
                )
            )
            data.append(
                (
                    carbon_prefix + "rtt.avg",
                    (timestamp, ping_result.latency_stat.rtt_avg),
                )
            )
            data.append(
                (
                    carbon_prefix + "rtt.mdev",
                    (timestamp, ping_result.latency_stat.rtt_mdev),
                )
            )
        data.append(
            (
                carbon_prefix + "loss.avg",
                (timestamp, ping_result.latency_stat.loss_avg),
            )
        )

    return data


async def send_raw_data(host: str, port: str, message: bytes):
    """Send raw data to a host with async"""
    _, writer = await asyncio.open_connection(host=host, port=port)
    writer.write(message)
    await writer.drain()
    writer.close()
    await writer.wait_closed()


async def send_results_to_carbon(
    carbon_host: str,
    carbon_port: str,
    config: PingerConfig,
    ping_results: List[PingResult],
    ip_family: str,
):
    """Send a PingsResult list to carbon."""
    data = build_data_for_carbon(config, ping_results, time.time(), ip_family)
    payload = pickle.dumps(data, protocol=2)
    header = struct.pack("!L", len(payload))
    message = header + payload

    try:
        await asyncio.wait_for(send_raw_data(carbon_host, carbon_port, message), 5)
    except asyncio.TimeoutError:
        logging.exception("Timeout while sending data to carbon!")
