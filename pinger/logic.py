"""
Where all the logic happens.
"""

import math
import logging
import statistics
import socket

import asyncio

from abc import ABC, abstractmethod
from typing import List, Optional

from pinger.dataclass import Host, PingResult, LatencyStat

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


class Pinger(ABC):
    """The Pinger class handle all the work of scheduling ping and extracting
    data out of it.
    """

    def __init__(self, protocol):
        self.protocol = protocol

    @abstractmethod
    async def simple_ping(
        self, host: str, timeout: float, family: socket.AddressFamily
    ) -> float:
        """The abstract method to implement probes."""

    @staticmethod
    def extract_latency_stat_from_bulk(pings: List[float], failed: int) -> LatencyStat:
        """Extract LatencyStat over a a sample of pings"""
        rtt_min: Optional[float] = None
        rtt_max: Optional[float] = None
        rtt_avg: Optional[float] = None
        rtt_mdev: Optional[float] = None
        loss_avg: float = 1

        if pings:
            median = statistics.median(pings)
            diffs_from_median = map(lambda x: abs(x - median), pings)

            rtt_min = min(pings)
            rtt_max = max(pings)
            rtt_avg = statistics.mean(pings)
            rtt_mdev = statistics.mean(diffs_from_median)
            loss_avg = failed / (failed + len(pings))

        return LatencyStat(
            rtt_min=rtt_min,
            rtt_max=rtt_max,
            rtt_avg=rtt_avg,
            rtt_mdev=rtt_mdev,
            loss_avg=loss_avg,
        )

    @staticmethod
    def _find_ipv6_address(host: str):
        try:
            socket.getaddrinfo(host + ".ipv6", port=None, family=socket.AF_INET6)
            host += ".ipv6"
        except socket.gaierror:
            pass

        return host

    async def bulk_single_host_ping(
        self,
        host: Host,
        timeout: float,
        pings_per_host: int,
        family: socket.AddressFamily,
    ) -> PingResult:
        """Do the sample of ping for a single host and return the corresponding
        PingResult.
        """
        pings: List[float] = []
        failed = 0

        address = host.address
        if family == socket.AF_INET6:
            address = Pinger._find_ipv6_address(host.address)

        for _ in range(pings_per_host):
            try:
                pings.append(
                    await self.simple_ping(address, timeout / pings_per_host, family)
                )
            except Exception:  # pylint: disable=broad-except
                failed += 1

        latency_stat = self.extract_latency_stat_from_bulk(pings, failed)
        return PingResult(host=host, latency_stat=latency_stat, protocol=self.protocol)

    async def bulk_multiple_hosts_ping(
        self,
        hosts: List[Host],
        timeout: float,
        pings_per_host: int,
        family: socket.AddressFamily,
    ) -> List[PingResult]:
        """Do the sample of ping for multiple hosts and return the corresponding
        list of PingResult.
        """
        tasks = []

        for host in hosts:
            tasks.append(
                self.bulk_single_host_ping(host, timeout, pings_per_host, family)
            )

        return await asyncio.gather(*tasks)

    async def ping(
        self,
        hosts: List[Host],
        timeout: int,
        max_bulk_size: int,
        pings_per_host: int,
        family: socket.AddressFamily,
    ) -> List[PingResult]:
        """Ping a list of hosts and get the corresponding list of PingResult."""
        res: List[PingResult] = []

        bulk_number = math.ceil(len(hosts) / max_bulk_size)
        timeout_per_group = timeout / bulk_number

        for i in range(0, len(hosts), max_bulk_size):
            host_groups = hosts[i : min(i + max_bulk_size, len(hosts))]
            res.extend(
                await self.bulk_multiple_hosts_ping(
                    host_groups, timeout_per_group, pings_per_host, family
                )
            )

        return res
